---
layout: page
title: Impressum
permalink: /impressum/
---

fluxcess GmbH  
Kollegienwall 1a  
49074 Osnabrück  


Telefon: +49 (0)541 952243-0


E-Mail: info@fluxcess.com  
WWW: www.fluxcess.com


Umsatzsteuer-Identifikationsnummer: DE304533232  
Handelsregister: Osnabrück HRB 210001  
Geschäftsführer: Daniel Chitralla
