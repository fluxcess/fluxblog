---
layout: page
title: Changelog
permalink: /changelog/
---

{% for post in site.posts %}
{% if post.categories contains "changelog" %}
{{ post.content }}
{{ post.date | date: "%d.%m.%Y" }}
{% endif %}
{% endfor %}