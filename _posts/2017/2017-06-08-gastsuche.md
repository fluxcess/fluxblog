---
layout: post
title:  "Gäste schöner suchen mit fluxcess"
slug:   admin-gast-suche
date:   2017-06-08 10:44:18 +0200
categories: gästeliste news
tags:   gästeliste suche
---

# Die Suchfunktion

Unsere Suchfunktion ist jetzt auch im Admin-Bereich Tip Top: sie sucht ab sofort im 
Vornamen, Nachnamen, Firmennamen, in der Gast Id und im Zugangscode. Dabei lassen sich 
nun - genau wie im Check-In schon seit langem - Kombinationen aus Teilwörtern suchen.

Was bedeutet das?

Nehmen wir an, Herr Müller ruft an. Sie verstehen am Telefon seinen Nachnamen - und tippen 
fleißig drauflos: Mül ... An diesem Punkt zeigt Ihnen die Software eine lange Liste von 
Personen mit dem Nachnamen "Müller" an. Ein Weitertippen ("ler") hätte wenig Sinn.

Glücklicherweise ist Ihnen nicht entgangen, dass Herr Müller eine Krawattennadel mit seinem 
Firmenlogo trägt (es ist die Superflux AG). Also tippen Sie - nach einem Leerzeichen: 
Sup ... an diesem Punkt zeigt die Software eine Liste von etwa 50 Personen mit Nachnamen 
"Müller" an, alle mit der Firma "Superflux AG". Weitertippen zwecklos.

Nun bleibt Ihnen nichts anderes übrig, als Ihren Gast nach dem Vornamen zu fragen... und 
tippen drauf los: Markus. Jetzt kann das System Ihren Gast eindeutig identifizieren.