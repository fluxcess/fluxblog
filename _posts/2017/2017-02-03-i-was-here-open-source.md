---
layout: post
title:  "Entwicklerglück: I Was Here Check-In Software jetzt Open Source verfügbar"
slug: i-was-here-check-in-software-open-source
date:   2017-02-03 08:45:21 +0200
categories: fluxcess software check-in news
tags:   fluxcess gästeliste i-was-here check-in software open-source
---

Unsere Check-In Software *I Was Here* haben wir heute Open Source auf Github veröffentlicht. 

Wir empfinden Open Source Software als besonders vertrauenswürdig und transparent. Sie können 
jederzeit Einsicht in den Quellcode erhalten, unabhängige Experten sind in der Lage, unsere Software 
zu prüfen. Vielleicht finden Sie - oder Ihre IT Sicherheitsexperten den einen oder anderen Bug. 
Wir freuen uns darüber, wenn Sie uns das mitteilen, denn dann können wir unsere Software optimieren 
und noch besser werden.

Die Entwicklung von Software ist zeit- und kostenintensiv. Wenn Sie ein Support-Paket bei uns 
erwerben, unterstützen Sie uns nicht nur bei der Entwicklung, sondern erhalten auch Hilfe, wenn einmal 
etwas schief geht - direkt vom Hersteller.

Wir wünschen viel Spaß mit *I Was Here*!

[fluxcess I Was Here Check-In Software auf Github](https://github.com/fluxcession/IWasHere).
