---
layout: post
title:  "Eingehender Datentransfer"
slug: datentransfer-eingehend
date:   2017-01-17 06:35:00 +0200
categories: fluxcess features news
tags:   fluxcess gästeliste datentransfer transfer import
---

Der Datentransfer von der fluxcess Cloud auf einen lokalen Check-In Server wurde mit dem letzten Release erheblich 
vereinfacht - und ist jetzt vom Benutzer über die Oberfläche des Check-In Servers möglich. Ein manuelles Eingreifen des 
fluxcess Servicepersonals ist in den meisten Fällen nicht mehr notwendig.

![Datentransfer Import](/assets/screenshot-datentransfer-in-2.png)

Die detaillierte Beschreibung eines Usecases zum *Datentransfer auf einen lokalen Server* findet sich in der 
[fluxcess Dokumentation](https://book.fluxcess.com/besuchermanagement/usecases/datentransfer-in).
