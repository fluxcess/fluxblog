---
layout: post
title:  "Die Check-In Software mit einem Dropdown-Feld verzieren - so geht's!"
slug:   check-in-form-dropdown
date:   2017-06-10 10:32:28 +0200
categories: check-in news
tags:   check-in
---

# Verzieren der Windows Check-In Software - jetzt auch mit Dropdown Feld!

Bei manchen Veranstaltungen ist es notwendig, den Gast während des Check-Ins eine Auswahl treffen zu lassen. Sei es, weil 
man nur an einem von 30 Workshops teilnehmen kann, weil eine Sprache ausgewählt werden soll, oder weil die Angabe einer 
Dachinstitution oder eines Verbandes notwendig ist.

In der *fluxcess* Windows Check-In Software gibt es jetzt die Möglichkeit, eine solche Auswahl per Dropdown-Feld zu treffen. 
Technisch werden die Raum- und Raumliste Funktionalitäten für dieses Feature genutzt. Neu in der Dokumentation ist eine 
genaue Anleitung zu diesem Use Case: [http://book.fluxcess.com/de/besuchermanagement/usecases/check-in-dropdown-feld/](http://book.fluxcess.com/de/besuchermanagement/usecases/check-in-dropdown-feld/).

Viel Erfolg!