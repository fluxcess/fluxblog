---
layout: post
title:  "Code Sicherheits-Prüfung"
slug:   sicherheits-check
date:   2017-07-16 11:01:28 +0200
categories: security news
tags:   security
---

# Sicherheitsprüfung der fluxcess Software - bitte aktualisieren Sie Ihre Installation!

Im Rahmen einer Qualitätsoffensive haben wir proaktiv einen externen Dienstleister mit einer Sicherheitsüberprüfung unserer 
Software beauftragt. Dieser hat festgestellt, dass es ein Problem mit der veralteten Datei *datatables.php* geben kann. 

Bitte entfernen Sie diese Datei manuell aus Ihrer fluxcess Installation. Sie ist für den Betrieb der Software nicht (mehr) 
notwendig. 

Die Sicherheitslücke kann nur mit Zugangsdaten ausgenutzt werden. Für Kunden, die den Admin-Bereich nur intern nutzen, 
dürfte sie kein Risiko darstellen. Kunden, die unsere Cloud-Lösung nutzen, sind nicht betroffen.

