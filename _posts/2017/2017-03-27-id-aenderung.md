---
layout: post
title:  "Änderung der ID-Policy: IDs jetzt nur noch alphanumerisch!"
slug: id-aenderung
date:   2017-03-27 11:00:01 +0200
categories: fluxcess software check-in news
tags:   fluxcess gästeliste check-in
---

Information: Die IDs, beispielsweise für Konferenz-Sessions oder Räume, müssen ab sofort alphanumerisch sein. 
Sonderzeichen sind nicht mehr möglich. Dieser Schritt war notwendig, um den Funktionsumfang von fluxcess 
noch weiter ausbauen zu können.

Wir wünschen einen guten Start in die Woche - und viel Erfolg mit fluxcess!