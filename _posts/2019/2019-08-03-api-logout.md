---
layout: post
title:  "Logout für Event Websites"
slug:   js-logout-fuer-rsvp
date:   2019-08-03 12:47:11 +0200
categories: api news
tags:   api authentifizierung rsvp
---

# Logout bei benutzerspezifischer Event-Seite mit Login über fluxcess RSVP

Wer die Login-Funktionalität der fluxcess RSVP API bisher für seine Veranstaltungswebsite nutzt, kann jetzt auch die Logout Funktionalität nutzen. 
Ein codebeispiel steht im 
<a href="http://book.fluxcess.com/en/dev/login_via_api_and_js/">Handbuch</a>
bereit.

Hierbei geht es wirklich nur um "Custom Websites", die normalerweise vollständig ohne fluxcess auskommen, aber auf einer oder zwei Seiten eine RSVP Funktion 
anbieten. Event-Registrierungen sind hiervon nicht betroffen.

Technisch hatten wir bei der Implementierung Schwierigkeiten mit 3rd Party Cookies. 
Am liebsten würden wir ganz auf Cookies verzichten; das scheint jedoch in einigen technischen Konstellationen nicht möglich zu sein, und würde mit Einschränkungen 
bei der Benutzerfreundlichkeit einhergehen. Den Cookie benötigen wir in Fällen, in denen es eine Login-Seite gibt, und auf einer anderen Seite 
das eigentliche RSVP Formular angezeigt wird. Ohne Cookie müsste der Besucher sich jedes mal einloggen, wenn er auf eine dieser beiten Seiten wechselt. 

Manche Browser lassen es nicht zu, dass ein Webservice wie fluxcess RSVP direkt über Ajax ein Cookie setzt. Wir behelfen uns daher, indem wir die Session Id in 
einer lokalen Javascript variable speichern und folgenden Ajax Requests einen fluxcess-Header hinzufügen, der die Session Id enthält.

Wie auch immer es technisch gelöst ist:

Haben Sie eine eigene Event-Website für Ihre Veranstaltung mit geladenen Gästen, können Sie Login-Formular und RSVP Formular von fluxcess einbinden. 
fluxcess RSVP teilt Ihrer Website mit, ob der aktuelle Besucher eingeladen und eingeloggt ist oder nicht - und Sie können den Gästen wertvolle 
Insider Informationen anzeigen. 

