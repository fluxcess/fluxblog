---
layout: post
title:  "fluxcess Architektur"
slug:   fluxcess-architektur
date:   2019-07-07 11:39:51 +0200
categories: news
tags:   architektur
---

# fluxcess Architektur

Im Entwicklerbereich des Handbuches steht jetzt ein Architektur-Diagramm bereit. Dieses veranschaulicht die Konstellation der
verschiedenen fluxcess Bereiche:

- Admin
- RSVP
- Tickets

Hier geht es zu den Informationen zur fluxcess Architektur im <a href="https://book.fluxcess.com/en/dev/architecture/">Handbuch</a>.

