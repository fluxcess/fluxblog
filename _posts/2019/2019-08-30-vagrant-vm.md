---
layout: post
title:  "Vagrant VM für Gästelisten-Software"
slug:   fluxcess-vagrant-vm
date:   2019-08-30 09:02:41 +0200
categories: vm developer
tags:   vm developer vagrant
---

# Installation der fluxcess Gästelisten-Software per virtueller Maschine

Ausprobieren, Entwickeln und Testen ist jetzt so einfach wie noch nie zuvor: 

Mit nur drei Kommandozeilenbefehlen kann jetzt der Admin-Bereich der Gästelisten-Software als 
virtuelle Maschine installiert werden.

Wir versprechen uns davon deutlich kürzere Entwicklungszeiten, bessere Software, einfachere 
Qualitätskontrolle und mehr Effizienz. - Manchmal muss man einfach etwas Aufwand in die Basics investieren.

Der Code für die VM ist natürlich Open Source und auf Gitlab verfügbar:
[https://gitlab.com/fluxcess/fluxcess_vm](https://gitlab.com/fluxcess/fluxcess_vm)

