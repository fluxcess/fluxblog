---
layout: post
title:  "Geladene Gäste - einfach mit fluxcess verwalten!"
date:   2015-11-20 13:34:23 +0200
permalink: /besuchermanagement/g%C3%A4steliste/2015/11/20/geladene-gaeste.html
categories: Besuchermanagement Gästeliste
---

Heute erreichte uns die erste Anfrage:

Der Kunde möchte eine gedruckte Einladung an geladene Gäste versenden.

![Einladung](/assets/serienbrief_einladung_fluxcess.png){:height="100px"}

Die Gäste sollen sich einloggen...

![Login Fenster](/assets/login-fenster.png){:height="100px"}

... und auswählen, ob sie an der Abendveranstaltung, am Frühstück danach – oder an beidem teilnehmen.

![Auswahl](/assets/auswahl-abendveranstaltung-fruehstueck.png){:height="250px"}

Die finale Liste möchte der Kunde exportieren können.

Kein Problem – wir liefern gerne die Registrierungs-Website und den Export. 
Kostenvoranschlag gab es schon am Telefon, Angebot und Vorschau-Präsentation folgten auf dem Fuß. 
Gästelisten für Events mit geladenen Gästen sind definitiv unsere Stärke!

