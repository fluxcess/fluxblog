---
layout: post
title:  "fluxcess Newsletter - jetzt abonnieren!"
date:   2015-12-15 17:44:17 +0200
permalink: /fluxcess/2015/12/15/newsletter-abonnieren.html
categories: fluxcess
tags:   fluxcess
---

Immer auf dem Laufenden zu Ticketing Software Lösungen mit dem fluxcess Newsletter - jetzt anmelden auf
[www.fluxcess.com]!

[www.fluxcess.com]: http://www.fluxcess.com
