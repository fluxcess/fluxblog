---
layout: post
title:  "fluxcess Headquarters im Coworking Space Osnabrück"
date:   2015-11-25 09:22:03 +0200
permalink: /2015/11/25/firmenschild-fluxcess-osnabrueck.html
categories: 
tags:   
---

fluxcess agiert ab sofort von Niedersachsen aus. Heute hat das Team von [Unter Kollegen Coworking](http://unterkollegen.de) 
unsere Firmenschilder angebracht. Wir freuen uns, einer der ersten Siedler im nagelneuen 
Coworking Space sein zu dürfen und freuen uns auf eine fluxcess-Story, die in der 
charmanten Friedensstadt [Osnabrück](https://de.wikipedia.org/wiki/Osnabrück) beginnt.

![fluxcess Firmenschild](/assets/firmenschild_2015_aussen.jpg){:height="100px"}

Das fluxcess Team hat sich am Standort seines Hauptquartiers bewusst für das fortschrittliche 
Konzept *Coworking* entschieden. Coworking steht für den dynamischen Umgang mit Technologien, 
Networking, offene Kommunikation und innovative Geschäftsmodelle - Eigenschaften, die auch die 
Marke *fluxcess* repräsentiert.

