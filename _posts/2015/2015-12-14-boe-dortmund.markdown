---
layout: post
title:  "20.+21. Januar: fluxcess auf der Best of Events 2016 in Dortmund"
date:   2015-12-14 07:31:58 +0200
permalink: /fluxcess/2015/12/14/boe-dortmund.html
categories: fluxcess
tags:   Ausstellung, Best of Events, BoE, Termine
---

Kaum gegründet - schon die erste Messebeteiligung. Möchten Sie uns gerne persönlich kennen lernen? 
Haben Sie Fragen zu einer Ticketing Lösung? Möchten Sie sich gerne von uns zeigen lassen, wie man 
zeitgemäß und günstig Eintrittskarten verkauft?

![fluxcess BoE 2016](/assets/fluxcess_boe2016_banner.png){:height="100px"}

Sie finden uns am 20. und 21. Januar 2016 in den Dortmunder Westfalenhallen, Halle 4, Stand A18. Wir 
demonstrieren Ticketing vom feinsten!


Wenn Sie schon wissen, dass Sie vorbeikommen möchten, schreiben Sie uns gerne eine E-Mail - damit 
wir Sie auf die Gästeliste setzen können. Außerdem gibt es die Möglichkeit, über das Matchmaking Tool 
der Best of Events schon im Vorfeld Termine zu planen. So ist sicher gestellt, dass wir genügend Zeit haben, 
um Sie adäquat beraten zu können.

Wir freuen uns darauf, Sie in Dortmund zu treffen!
