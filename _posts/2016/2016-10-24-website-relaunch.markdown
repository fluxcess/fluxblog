---
layout: post
title:  "fluxcess Website Facelift"
date:   2016-10-24 17:17:17 +0200
permalink: /fluxcess/news/2016/10/24/website-relaunch.html
categories: fluxcess news
tags:   fluxcess website
---


Schön &amp; schön schnell: Die fluxcess Website lädt jetzt flux schnell und ist gleichzeitig fluxus simpel - 
ohne Schnickschnack, aber mit den wichtigsten Informationen. Feedback? Kritik? Gerne!

<a href="http://www.fluxcess.com">www.fluxcess.com</a>
