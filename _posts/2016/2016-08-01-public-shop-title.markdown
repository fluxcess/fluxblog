---
layout: post
title:  "Optimierung: Titel im Shop jetzt umstellbar"
date:   2016-08-01 23:01:11 +0200
permalink: /fluxcess/features/2016/08/01/public-shop-title.html
categories: fluxcess features
tags:   fluxcess feature ticketshop
---


Zu unserer Überraschung nutzen erste Kunden fluxcess nicht nur für 
Ticketbuchungen, sondern auch, um beispielsweise Sponsorenpakete, 
Partnerleistungen oder Werbeleistungen für ihr Event zu verkaufen. 

So kann die bestehende Infrastruktur mit SSL-Verschlüsselung und 
kundenspezifischem Shop optimal genutzt werden. 

Zu Irritationen führte in diesem Zusammenhang bisher, dass im 
kundenspezifischen Shop 
jedes Event mit dem Satz "Tickets kaufen: " + Eventname betitelt war. 
Zukünftig ist diser Titel im Shop frei wählbar: Als Standardtext 
wird weiterhin die Kombination aus "Tickets kaufen" und dem Event-Titel 
angezeigt - diese lässt sich jedoch jederzeit ändern, beispielsweise in 
"Registrierung", "Buchung" oder beliebige andere Texte.


![Shop Überschrift](/assets/screenshot_shop_headline.png){:height="300px"}

Die neue Einstellung findet sich im Menüpunkt "Online Eventliste", 
ist ab sofort aktiv und kann bereits genutzt werden.
