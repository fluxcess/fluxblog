---
layout: post
title:  "Neuer Export: Konferenz Sessions"
date:   2016-07-23 08:13:20 +0200
permalink: /fluxcess/features/2016/07/23/conferencesession-export.html
categories: fluxcess features
tags:   fluxcess feature konferenz session
---


Ihre Konferenz hat ein umfangreiches Programm mit verschiedenen Strängen - vielleicht nach Themen geordnet, 
möglicherweise über mehrere Tage verteilt. Jeder Konferenzteilnehmer darf an verschiedenen Sessions, Vorträgen 
oder Breakouts teilnehmen.

Wussten Sie, dass fluxcess nicht nur ein praktisches Auswahltableau für diese Sessions verfügt, sondern die 
Auswahl neuerdings auch direkt exportiert werden kann? 

![Konferenz Session Export](/assets/screenshot_fluxcess_export_sessions.png){:height="100px"}

Falls auch Sie mit Sessions arbeiten, lohnt es sich, dieses nagelneue Feature im Import/Export Menü einmal 
auszuprobieren. Die bisherige, etwas komplizierte Zählweise innerhalb Ihres Tabellenkalkulationsprogrammes 
ist nun nicht mehr nötig.

Der Export liefert die Anzahl der gebuchten Teilnehmer pro Session gleich mit, sodass Sie sich voll auf die 
Auswertung und Raumbuchung konzentrieren können. Viel Spaß damit!
