---
layout: post
title:  "Anzeige des Rechnungsbetrages"
date:   2016-11-01 10:48:03 +0200
permalink: /fluxcess/features/ticketing/kongress/2016/11/01/rechnung-info.html
categories: fluxcess features ticketing kongress
tags:   fluxcess rechnungsstellung
---

Welcher Betrag wurde dem Gast berechnet? Welchen Umsatz hat der Gast generiert?

Um diese Frage beantworten zu können, war es bisher notwendig, die Rechnung als 
PDF-Dokument herunterzuladen oder in einem der Buchhaltungsexporte nachzusehen. 
Hier sind selbstverständlich alle Beträge aufgeführt.

Dies führte jedoch zu kurzen Wartezeiten bei der Nutzung von fluxcess als 
Kongress-Software auf Veranstaltungen:  
Traf ein Gast ein, dessen Rechnung als "nicht bezahlt" markiert war, 
musste das PDF-Dokument geöffnet werden, um den offenen Betrag auslesen zu können. 
Hierbei gingen wertvolle Sekunden verloren.

Zur Beschleunigung derartiger Check-In Prozesse mit Zahlung haben wir die 
Rechnungsübersicht pro Kunde um eine Brutto/Netto Betrags-Angabe 
ergänzt. So wird auf einen Blick sichtbar, wie viel dem jeweiligen Gast 
berechnet wurde.

![Der Rechnungsbetrag wird neben der Rechnung angezeigt.](/assets/rechnungsbetrag.png)

Das neue Feature wird mit dem nächsten Deployment aktiv und steht für Kunden 
mit On-Premise Systemen zum Ende des Monats als automatisches Update bereit.
