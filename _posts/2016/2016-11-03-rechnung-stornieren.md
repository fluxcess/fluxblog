---
layout: post
title:  "Rechnungen stornieren"
slug: rechnungen-stornieren
date:   2016-11-03 00:03:44 +0200
categories: fluxcess features ticketing
tags:   fluxcess rechnungsstellung storno rechnung
---

Eine Rechnung doppelt ausgestellt? Dem Gast etwas falsches berechnet? Kein Problem!

Rechnungen können in der fluxcess Software nun per Mausklick storniert werden.

**Gelöscht** werden diese Rechnungen selbstverständlich **nicht**. Sie werden 
weiterhin angezeigt und als *storniert* markiert. 

Die Software 
versieht normalerweise alle nicht bezahlten Rechnungen mit einem Warnhinweis. 
Stornierte Rechnungen jedoch erhalten den Warnhinweis nicht. Dies führt zu mehr Übersicht, 
insbesondere im Response Handling und beim Besuchermanagement: 
Welche Gäste noch Rechnungen zahlen müssen, ist auf einen Blick sichtbar.

Das neue Feature wird mit dem nächsten Deployment aktiv und steht für Kunden 
mit On-Premise Systemen zum Ende des Monats als automatisches Update bereit.