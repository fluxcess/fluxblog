---
layout: post
title:  "Ticketverkauf für Ihr Event: Frühbucherpreise"
date:   2016-07-18 12:57:51 +0200
permalink: /fluxcess/features/2016/07/18/fruehbucherpreise.html
categories: fluxcess features
tags:   fluxcess feature tickets preise
---


Schon gewusst? - Preiskategorien und Eintrittskarten-Kategorien können Sie bei fluxcess mit Verkaufsstart- und 
-enddaten versehen. Wenn Ihr Frühbucherpreis beispielsweise bis zum 15. September gültig ist, dann können Sie dieses 
Enddatum für den Verkauf von Karten zum Frühbucherpreis problemlos konfigurieren - und den Verkaufszeitraum für 
Karten zum Normalpreis mit dem 16. September beginnen lassen. 

Selbstverständlich ist es auch möglich, mehr als nur Frühbucherpreise und Normalpreise anzugeben - so wäre 
beispielsweise noch Platz für Last Minute- oder Super-Frühbucher-Kategorien oder Gutscheine, die ab oder bis zu einem 
bestimmten Tag gültig sind.

In Ihrem an das fluxcess System angebundenen Firmen-Ticketverkaufsportal werden selbstverständlich automatisch immer nur 
Karten zum aktuell verfügbaren Preis angeboten.
