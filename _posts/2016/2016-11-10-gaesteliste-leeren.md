---
layout: post
title:  "Gästeliste leeren"
slug: gaesteliste-leeren
date:   2016-11-10 12:35:53 +0200
categories: fluxcess features
tags:   fluxcess gästeliste api testen
---

Einer unserer Lieblingskunden hat sich gewünscht, dass man die Gästeliste für ein Event per 
Mausklick (oder besser noch per API) leeren kann.

Er testet regelmäßig Veranstaltungen ausführlich vorab, hätte dann zum Beginn des Events jedoch 
gerne eine leere Gästeliste, ganz ohne Max-Mustermann-Datensätze.

Das Feature wurde selbstverständlich in fluxcess integriert: Im Menüpunkt __Aktionen__ gibt 
es fortan eine Funktion **Alle Gast-Daten löschen**. Diese entfernt alle Gäste aus der Gästeliste 
und die dazugehörigen Tickets bzw. Rechnungen.

Das neue Feature ist bereits aktiv und voll einsatzfähig.