---
layout: post
title:  "Neues Feature: Dateimanager"
date:   2016-07-08 15:04:21 +0200
permalink: /fluxcess/features/2016/07/08/dateimanager.html
categories: fluxcess features
tags:   fluxcess feature datei
---


Wir präsentieren mit unserem nagelneuen Dateimanager einen weiteren Meilenstein auf dem Weg zur optimalen Software 
für Besuchermanagement: 

![Dateimanager](/assets/screenshot_dateimanager.png)

Alle Dokumente, die mit dem Dateimanager verwaltet werden, können ausschließlich durch eingeloggte 
Firmenmitglieder eingesehen werden. Ein öffentlicher Zugriff auf diese Dokumente ohne Login ist nicht möglich.

Der Dateimanager wird zukünftig beispielsweise für folgende Anwendungen verwendet:

* PDF Dokument als Hintergrund-Layer für Rechnungen oder Tickets (derzeit als Beta-Funktionalität verfügbar)
* Icons, Bilder und Zertifikate für die Apple Wallet Integration (derzeit als Beta-Funktionalität verfügbar)

