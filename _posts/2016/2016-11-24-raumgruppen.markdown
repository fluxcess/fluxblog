---
layout: post
title:  "Räume und Raumgruppen"
slug: raum-raumgruppe-vortrag
date:   2016-11-24 11:52:00 +0200
categories: fluxcess features
tags:   fluxcess gästeliste raum gruppe
---

Unser neues Feature "Raumgruppen" ermöglicht es, mehrere Räume zu einer Raumgruppe zu verbinden. 
Ein Raum kann dabei zu mehreren Raumgruppen gehören.

Die Verwaltung erfolgt über den neuen Menüpunkt _Einstellungen_ > _Raumgruppe_ im Eventbereich:

![Raumgruppe](/assets/raumgruppe.png)

Die Anwendung von Raumgruppen beschränkt sich derzeit auf die Check-In Software, in der Teilnehmer 
beispielsweise während des Check-Ins einen Raum aus einer Raumgruppe auswählen können. 

Eine mögliche Anwendung ist auch die Auswahl eines Seminares, Workshops oder Vortrages aus einer Gruppe von Seminaren, 
Workshops oder Vorträgen.

Die detaillierte Beschreibung eines Usecases zur *Raumauswahl* findet sich in der 
[fluxcess Dokumentation](https://book.fluxcess.com/besuchermanagement/usecases/raumwahl).
